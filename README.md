# IoT Interoperation Repository

Welcome to the repository dedicated to addressing the challenges and solutions in IoT heterogeneity and interoperation. This repository houses research findings that delve into the evolving landscape of IoT, particularly focusing on seamless application provisioning over heterogeneous IoT protocols. 

## Overview
IoT is developing rapidly with frequently appearing new wireless standards and applications. However, besides a large number of IoT benefits, its further development is now being slowed down due to the repetition of old Internet development flaws while dealing with IoT heterogeneity. The current misleading trend aims to solve all IoT interoperation problems by inserting IP Addresses into those wireless protocols where the IP stack clearly slows down application performance and drains the battery, e.g. LPWANs such as LoRaWAN and SigFox. This paper tackles IoT heterogeneity from a different perspective: it is the application interoperation which matters the most. The protocols beneath the application layer shall work for smooth upper-layer service provisioning where the IP shall be just one of the many underlying integration options instead of being the essential one. Inspired by previous proposals for a more flexible internetworking architecture, this paper applies those theoretical concepts in practice by proposing a protocol-independent distributed interoperation model for smooth service provisioning over heterogeneous IoT wireless contexts. The arguments pro the IP-agnostic IoT application interoperation are supported by the model's prototype which showed 1.6-4 times faster MQTT application operation over LoRa and WiFi compared to the legacy IP-based MQTT provisioning over that protocols.

The repository contains the source code of the research paper "Towards Seamless and Protocol-Independent IoT Application Interoperability" (published at HotNets '21) and the decentralised IP-agnostic IoT interoperation model. Please navigate to ```IoT_interop``` directory containing source code details. 

## Authors
Vadim Safronov (main author), Justas Brazauskas, Matthew Danish, Rohit Verma, Ian Lewis, Richard Mortier


## Contact
For inquiries, collaborations, or further discussions, please contact Vadim Safronov vs451@cam.ac.uk.


## License
This work is licensed under a Creative Commons Attribution International 4.0 License.

