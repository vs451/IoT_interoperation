## Source code

The IoT interoperation repository contains two zip files with different source code contents:
- ```IoT_interoperation_paper.zip``` contains the paper latex source code
- ```code.zip``` contains the code of IP-agnostic decentralised model and deployment use case

## Running the IoT interoperation model

1. Download and unzip code.zip. The folder contains the following directories:
    - **Controller LoRa**: LoRa controller, runs on pycom devices (tested on LoPy4).
    - **Controller WiFi**: WiFi controller, runs on any linux-based Raspberry Pi with paho mqtt library installed (tested on Raspberry Pi OS).
    - **SGW**: Smart Gateway with an interstitial function for seamless MQTT interoperation between LoRa and WiFi contexts. Runs on pycom devices (tested on LoPy4).
    - **Sensor A**: battery-powered sensor A. Runs on pycom devices (tested on LoPy4).
    - **Sensor B**: socket-powered sensor B. Runs on pycom devices (tested on LoPy4).
    - **Sensor C**: socket-powered sensor C. Runs on any linux-based Raspberry Pi with paho mqtt library installed (tested on Raspberry Pi OS).
2. Download each folder to the corresponding device. For pycom devices edit config.py by replacing SSID, WiFi password and all IP addresses with your network setting values.
3. To run the code on each device, execute the main ```.py``` file in the device directory. E.g. to run the SGW code you need to execute ```SGW.py```
